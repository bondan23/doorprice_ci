<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Import extends Backend_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('excel');
    }
    
    public function index()
    {
        $this->load->view('secure/upload');
    }
    
    public function send()
    {
        $this->output->unset_template();
        $this->load->model('peserta_m');
        
        $config['upload_path'] = './assets/excel/';
		$config['allowed_types'] = 'xls|xlsx';
		$this->load->library('upload',$config);
        
        if($this->upload->do_upload('userfile'))
		{
            $inputFileName = realpath($config['upload_path'].$this->upload->file_name);
			
			$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFileName);
			
			foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
				$arrayData[$worksheet->getTitle()] = $worksheet->toArray();
			}
            
			$worksheet = $this->input->post('worksheet');
            
            $sheet = $worksheet;
            $kategori = range("A","D");
            
            foreach($arrayData[$sheet] as $k=>$v)
            {
                $nama = $v[0];
                $nik = $v[1];
                $dept = $v[2];
                $kategori = $v[3];
                
                if($k > 0 && $nama != null)
                {
                    $insert = array(
                        "nik" => $nik,
                        "nama" => $nama,
                        "kategori" => $kategori,
                        "department"=>$dept
                    );
                    
                    
                    $this->peserta_m->insert($insert);
                }
            }
            
            redirect('secure/import/data_peserta');
        }
        else
		{
			echo  $this->upload->display_errors();
		}
    }
    
    public function data_peserta()
    {
        if($this->input->is_ajax_request())
        {
            $this->output->unset_template();
            if($this->input->server('REQUEST_METHOD') == 'GET')
            {
                if(isset($_GET['q']))
                {
                    $peserta = $this->peserta_m->search($this->input->get('q'));
                    echo json_encode(array("data"=>$peserta));
                }
                else{
                    $peserta = $this->peserta_m->get_all();
                    echo json_encode(array("data"=>$peserta));
                }
            }
        }
        else{
            $this->_jqwidget();
            $this->load->js('assets/js/system_script.js');
            $this->load->view('secure/data_peserta');
        }
    }
}