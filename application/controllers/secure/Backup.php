<?php

class Backup extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->dbutil();
    }
    
    public function index()
    {
        $prefs = array(
                'tables'      => array(),  // Array of tables to backup.
                'ignore'      => array(),           // List of tables to omit from the backup
                'format'      => 'txt',             // gzip, zip, txt
                'filename'    => 'mybackup.sql',    // File name - NEEDED ONLY WITH ZIP FILES
                'add_drop'    => TRUE,              // Whether to add DROP TABLE statements to backup file
                'add_insert'  => TRUE,              // Whether to add INSERT data to backup file
                'newline'     => "\n"               // Newline character used in backup file
          );

        $backup = $this->dbutil->backup($prefs); 
        
        $this->load->helper('download');
        force_download('mybackup.sql', $backup); 
    }
}