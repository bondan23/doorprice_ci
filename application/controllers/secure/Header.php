<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Header extends Backend_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->js('assets/js/system_utility.js');
    }
    
    public function index()
    {
        $header = $this->header_m->get_all();
        foreach($header as $v){
            $data[$v['tipe']] = $v['text'];
        }
        
        $this->load->view('secure/header',$data);
    }
    
    public function edit()
    {
        $this->output->unset_template();
        $tipe = $this->input->post('pk');
        $text = $this->input->post('value');
        $this->header_m->update($tipe,array('text'=>$text));
    }
}