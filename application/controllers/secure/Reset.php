<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Reset extends Backend_Controller
{
    
    public function __construct()
    {
        parent::__construct();
        $this->load->library('seeder');
        $this->load->helper('file');
    }
    
    public function index()
    {
        $hadiah = $this->hadiah_m->get_all();
        $peserta = $this->peserta_m->get_all();
        $data['peserta'] = $peserta;
        $data['hadiah'] = $hadiah;
        
        $this->load->section('modal','modal');
        $this->load->js('assets/js/modal.js');
        $this->load->view('secure/reset',$data);
    }
    
    public function reset_table($table)
    {
        $this->output->unset_template();
        $data = 'false';
        write_file('./assets/konfirmasi.txt', $data, 'w');
        
        $this->db->truncate($table);
        $this->db->truncate('pemenang');
        $this->session->set_flashdata('success','Sukses Me-Refresh Ulang Data '.ucfirst($table));
        
       redirect('secure/dashboard');
    }
    
    public function reset_all()
    {
       $this->output->unset_template();
       $data = 'false';
       write_file('./assets/konfirmasi.txt', $data, 'w');
        
       $list =  $this->db->list_tables();
       foreach($list as $v)
       {
           if($v != 'migrations')
           {
               $this->db->truncate($v);
           }
       }
       delete_files('./assets/excel/',TRUE,TRUE);
       $this->seeder->call('SeederHeader');
       
       $this->session->set_flashdata('success','Sukses Me-Refresh Ulang Semua Content');
        
       redirect('secure/dashboard');
    }
}
    