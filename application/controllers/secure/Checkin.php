<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Checkin extends Backend_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function index()
    { 
        $this->_jqwidget();
        $this->load->js('assets/js/checkin.js');
        $this->load->view('secure/checkin');
    }
    
    public function check($param=null)
    {
        $this->output->unset_template();
        $a = $this->peserta_m->get_by(array('nik'=>$param));
        if(!empty($a))
        {
            $this->peserta_m->update($a['id'],array('hadir'=>true));
            echo json_encode(array('status'=>true));
        }
        else{
            echo json_encode(array('status'=>false));
        }
    }
    
    public function peserta_hadir()
    {
        $this->output->unset_template();
        $a = $this->peserta_m->order_by('updated_at','desc')->get_many_by(array('hadir'=>true));
        echo json_encode(array("data"=>$a));
    }
}