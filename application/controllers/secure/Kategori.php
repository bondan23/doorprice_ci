<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends Backend_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->_jqwidget();
        $this->load->js('assets/js/script_kategori.js');
    }
    
    public function index()
    {
        $kategori = $this->kategori_m->get_all();
        $this->load->view('secure/input_kategori',array("kategori"=>$kategori));
    }
    
    public function data_kategori()
    {
        if($this->input->is_ajax_request())
        {
            $this->output->unset_template();
            $data = $this->kategori_m->get_all();
            echo json_encode(array("data"=>$data));
        }
    }
    
    public function post()
    {
    	$this->output->unset_template();
    	$kategori = $this->input->post('nama_kategori');
    	$post = array(
    		'kategori'=>$kategori
    	);
    	$db = $this->kategori_m->insert($post);
        if($db){
            echo json_encode(array("status"=>true,"msg"=>"Sukses Upload"));
        }else{
            echo json_encode(array("status"=>false,"msg"=>"Duplicate Kategori"));
        }
    }
    
    public function delete()
    {
        $this->output->unset_template();
        $id= $this->input->post('id');
        $this->kategori_m->delete($id);
        return true;
    }
}