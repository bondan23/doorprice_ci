<?php

class Download extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('download');
    }
    
    public function index()
    {
        force_download('./assets/template/TemplateImport.xlsx', null); 
    }
}