<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hadiah extends Backend_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->_jqwidget();
        $this->load->js('assets/js/jquery.cropit.js');
        $this->load->js('assets/js/script_hadiah.js');
    }
    
    public function index()
    {
        $kategori = $this->kategori_m->get_all();
        $this->load->view('secure/input_hadiah',array("kategori"=>$kategori));
    }
    
    public function data_hadiah()
    {
        if($this->input->is_ajax_request())
        {
            $this->output->unset_template();
            $data = $this->hadiah_m->get_all();
            echo json_encode(array("data"=>$data));
        }
    }
    
    public function post()
    {
        $this->output->unset_template();
        $post = $_POST;
        
        if($post['image-data'] == null && $post['method'] != 'post')
        {
            unset($post['method']);
            unset($post['image-data']);
            $kategori ="";
            foreach($post['kategori'] as $v)
            {
                $kategori.=$v.",";
            }

            unset($post['kategori']);
            $post['kategori'] = rtrim($kategori,",");
            
            $id = $this->input->post('id');
            $this->hadiah_m->update($id,$post);
            echo json_encode(array("status"=>true,"msg"=>"Sukses Uploads"));
        }
        else{
            if($post['method'] == "post")
            {
                define('UPLOAD_DIR', 'assets/hadiah/');
                $img = $post['image-data'];
                $img = str_replace('data:image/png;base64,', '', $img);
                $img = str_replace(' ', '+', $img);
                $data = base64_decode($img);
                $file = UPLOAD_DIR . uniqid() . '.png';
                $success = file_put_contents($file, $data);
                unset($post['method']);
                unset($post['image-data']);
                
                $kategori ="";
                foreach($post['kategori'] as $v)
                {
                    $kategori.=$v.",";
                }
                
                unset($post['kategori']);
                $post['kategori'] = rtrim($kategori,",");
                $post['images'] = $file;
                $this->hadiah_m->insert($post);
                echo json_encode(array("status"=>true,"msg"=>"Sukses Upload"));
            }
            else{
                //edit
                define('UPLOAD_DIR', 'assets/hadiah/');
                $img = $post['image-data'];
                $img = str_replace('data:image/png;base64,', '', $img);
                $img = str_replace(' ', '+', $img);
                $data = base64_decode($img);
                $file = UPLOAD_DIR . uniqid() . '.png';
                $success = file_put_contents($file, $data);
                unset($post['method']);
                unset($post['image-data']);
                
                $kategori ="";
                foreach($post['kategori'] as $v)
                {
                    $kategori.=$v.",";
                }
                
                unset($post['kategori']);
                $post['kategori'] = rtrim($kategori,",");
                $id = $this->input->post('id');
                $post['images'] = $file;
                $this->hadiah_m->update($id,$post);
                echo json_encode(array("status"=>true,"msg"=>"Sukses Upload"));
            }
        }
        
        /*$config['upload_path'] = './assets/hadiah/';
		$config['allowed_types'] = 'jpg|jpeg|png';
		$this->load->library('upload',$config);
        $post = $_POST;
        
        if($_FILES['userfile']['name'] == null && $post['method'] != 'post')
        {
            unset($post['method']);
            $id = $this->input->post('id');
            $this->hadiah_m->update($id,$post);
            echo json_encode(array("status"=>true,"msg"=>"Sukses Upload"));
        }
        else{
            if($this->upload->do_upload('userfile'))
            {
                $post = $_POST;

                if($post['method'] == "post")
                {
                    unset($post['method']);
                    $post['images'] = base_url()."assets/hadiah/".$this->upload->file_name;
                    $this->hadiah_m->insert($post);
                    echo json_encode(array("status"=>true,"msg"=>"Sukses Upload"));
                }
                else{
                    unset($post['method']);
                    $id = $this->input->post('id');
                    $post['images'] = base_url()."assets/hadiah/".$this->upload->file_name;
                    $this->hadiah_m->update($id,$post);
                    echo json_encode(array("status"=>true,"msg"=>"Sukses Upload"));
                }
            }
            else
            {
                echo  json_encode(array("status"=>false,"msg"=>$this->upload->display_errors()));
            }
        }*/
    }
    
    public function set_hadiah_utama()
    {
        $this->output->unset_template();
        $id= $this->input->post('id');
        $check = $this->hadiah_m->get_by(array('hadiah_utama'=>1));
       
        if($check != null)
        {
            $this->hadiah_m->update($check['id'],array('hadiah_utama'=>0));
            $this->hadiah_m->update($id,array('hadiah_utama'=>1));
        }
        else{
            $this->hadiah_m->update($id,array('hadiah_utama'=>1));
        }
    }
    
    public function delete()
    {
        $this->output->unset_template();
        $id= $this->input->post('id');
        $this->hadiah_m->delete($id);
        return true;
    }
}