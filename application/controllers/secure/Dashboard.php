<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Backend_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function index()
    {
        $peserta = $this->peserta_m->get_all();
        $data['hitung_peserta'] = count($peserta);
        
        $this->load->view('secure/dashboard',$data);
    }
    
    public function logout()
    {
       $_SESSION['logout'] = true;
       redirect('secure');
    }
}