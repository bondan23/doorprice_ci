<?php

class Tools extends CI_Controller {

    public function __construct() {
        parent::__construct();

        // can only be called from the command line
        if (!$this->input->is_cli_request()) {
            exit('Direct access is not allowed. This is a command line tool, use the terminal');
        }

        $this->load->dbforge();

        // initiate faker
        $this->faker = Faker\Factory::create("id_ID");
    }

    public function message($to = 'World') {
        echo "Hello {$to}!" . PHP_EOL;
    }

    public function help() {
        $result = "The following are the available command line interface commands\n\n";
        $result .= "php index.php tools migration \"file_name\"         Create new migration file\n";
        $result .= "php index.php tools migrate [\"version_number\"]    Run all migrations. The version number is optional.\n";
        $result .= "php index.php tools seeder \"file_name\"            Creates a new seed file.\n";
        $result .= "php index.php tools seed \"file_name\"              Run the specified seed file.\n";
        $result .= "php index.php tools refresh \"table_name\"          Refresh/Truncate table.\n";
        $result .= "php index.php tools make_model \"file_name\"        Create a new model.\n";

        echo $result . PHP_EOL;
    }

    public function migration($name) {
        $this->make_migration_file($name);
    }
    
    public function make_model($name)
    {
        $this->generate_model($name);
    }

    /*Version of the migration file*/
    public function migrate($version = null) {
        $this->load->library('migration');

        if ($version != null) {
            if ($this->migration->version($version) === FALSE) {
                show_error($this->migration->error_string());
            } else {
                echo "Migrations run successfully" . PHP_EOL;
            }

            return;
        }

        if ($this->migration->latest() === FALSE) {
            show_error($this->migration->error_string());
        } else {
            echo "Migrations run successfully" . PHP_EOL;
        }
    }

    public function seeder($name) {
        $this->make_seed_file($name);
    }

    public function seed($name) {
        require_once APPPATH . "libraries/Seeder.php";
        $seeder = new Seeder();

        $seeder->call($name);
    }
    
    public function refresh($table=null)
    {
        try{
            if($table == null)
            {
                throw new Exception('Masukan Nama Tabel Yang Akan Di REFRESH !!'. PHP_EOL);
            }
            else{
                $this->db->truncate($table);
                echo "Table:$table, berhasil di refresh" . PHP_EOL;
            }
        }
        catch(Exception $e)
        {
            echo $e->getMessage();
        }
    }
    
    protected function generate_model($name)
    {
        $model_name = ucfirst($name)."_m";
        $table_name = strtolower($name);
        
        $path = APPPATH . "models/$model_name.php";

        $my_model = fopen($path, "w") or die("Unable to create model file!");
        
        $model_template = "<?php
        
class $model_name extends MY_Model
{
    protected \$_table = '$table_name';
    protected \$primary_key = 'id';
    protected \$return_type = 'object';
    
    /**
     * Support for soft deletes and this model's 'deleted' key
     */
    protected \$soft_delete = FALSE;
    protected \$soft_delete_key = 'deleted'; // if soft_delete == true | harus ada field deleted di database
    protected \$_temporary_with_deleted = FALSE; // keluarkan semua data meskipun statusnya soft deleted
    protected \$_temporary_only_deleted = FALSE; // keluarkan data yang hanya sudah di softdelete
    
    /**
     * The various callbacks available to the model. Each are
     * simple lists of method names (methods will be run on \$this).
     */
    protected \$before_create = array('created_at','updated_at');
    protected \$after_create = array();
    protected \$before_update = array('updated_at');
    protected \$after_update = array();
    protected \$before_get = array();
    protected \$after_get = array();
    protected \$before_delete = array();
    protected \$after_delete = array();
}
        ";
        
        fwrite($my_model, $model_template);

        fclose($my_model);

        echo "$path model has successfully been created." . PHP_EOL;
    }
    
    protected function make_migration_file($name) {
        $date = new DateTime();
        $timestamp = $date->format('YmdHis');

        $table_name = strtolower($name);

        $path = APPPATH . "database/migrations/$timestamp" . "_" . "$name.php";

        $my_migration = fopen($path, "w") or die("Unable to create migration file!");

        $migration_template = "<?php

class Migration_$name extends CI_Migration {

    public function up() {
        \$this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            )
        ));
        \$this->dbforge->add_key('id', TRUE);
        \$this->dbforge->create_table('$table_name');
    }

    public function down() {
        \$this->dbforge->drop_table('$table_name');
    }

}";

        fwrite($my_migration, $migration_template);

        fclose($my_migration);

        echo "$path migration has successfully been created." . PHP_EOL;
    }

    protected function make_seed_file($name) {
        $path = APPPATH . "database/seeds/$name.php";

        $my_seed = fopen($path, "w") or die("Unable to create seed file!");

        $seed_template = "<?php

class $name extends Seeder {

    private \$table = 'users';

    public function run() {
        \$this->db->truncate(\$this->table);

        //seed records manually
        \$data = [
            'user_name' => 'admin',
            'password' => '9871'
        ];
        \$this->db->insert(\$this->table, \$data);

        //seed many records using faker
        \$limit = 33;
        echo \"seeding \$limit user accounts\";

        for (\$i = 0; \$i < \$limit; \$i++) {
            echo \".\";

            \$data = array(
                'user_name' => \$this->faker->unique()->userName,
                'password' => '1234',
            );

            \$this->db->insert(\$this->table, \$data);
        }

        echo PHP_EOL;
    }
}
";

        fwrite($my_seed, $seed_template);

        fclose($my_seed);

        echo "$path seeder has successfully been created." . PHP_EOL;
    }

}
