<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
        // initiate faker
    }
    
	public function index()
	{
        $this->load->css('assets/css/preloader.css');
        $this->load->js('assets/js/init.js');
        
        //load section
        $this->load->section('loader','loader');
        
		$this->load->view('dashboard');
        
        $this->faker = Faker\Factory::create("id_ID");
	}
    
    public function hasil_undi()
    {
        //css
		$this->load->css('assets/jqwidgets/styles/jqx.base.css');
		
		//js core
		$this->load->js('assets/jqwidgets/jqxcore.js');
		$this->load->js('assets/jqwidgets/jqxdata.js');
		//js extra
		$this->load->js('assets/jqwidgets/jqxbuttons.js');
		$this->load->js('assets/jqwidgets/jqxscrollbar.js');
		$this->load->js('assets/jqwidgets/jqxmenu.js');
		$this->load->js('assets/jqwidgets/jqxlistbox.js');
		$this->load->js('assets/jqwidgets/jqxcheckbox.js');
		$this->load->js('assets/jqwidgets/jqxdropdownlist.js');
		$this->load->js('assets/jqwidgets/jqxgrid.js');
		$this->load->js('assets/jqwidgets/jqxinput.js');
		$this->load->js('assets/jqwidgets/jqxwindow.js');
		$this->load->js('assets/jqwidgets/jqxgrid.sort.js');
		$this->load->js('assets/jqwidgets/jqxgrid.pager.js');
		$this->load->js('assets/jqwidgets/jqxgrid.edit.js');
		$this->load->js('assets/jqwidgets/jqxgrid.selection.js');
		$this->load->js('assets/jqwidgets/jqxgrid.columnsresize.js');
		$this->load->js('assets/jqwidgets/jqxdata.export.js');
		$this->load->js('assets/jqwidgets/jqxgrid.export.js');
		$this->load->js('assets/js/script.js');
        
        if($this->uri->segment(4) == 'utama'){
            $this->load->helper('file');
            $string = read_file('./assets/konfirmasi.txt');
            if($string == "false")
            {
                $this->load->section('modal','modal_hadiah_utama',array('hadiah_id'=>$this->uri->segment(3)));  
            }
        }
        
        $this->load->view('detail');
    }

    public function get_pemenang_hadiah_utama(){
        $this->output->unset_template();
        $cek = $this->pemenang_m->with('peserta')->get_many_by(array('hadiah_id'=>$this->uri->segment(3),'status'=>0));
        echo json_encode($cek);
    }
    
    public function export()
    {
        $this->output->unset_template();
        $this->load->library('table');
        $xml=simplexml_load_string($_POST['content']) or die("Error: Cannot create object");
        //debugger(($xml->Worksheet->Table->Row));
        $data = $xml->Worksheet->Table->Row;
        
        $index = 0;
        $nomor =1;
        foreach($data as $key=>$val)
        {
            foreach($val as $k=>$v)
            {
                $values = $v->Data[0]->__toString();
                if($values == null)
                {
                    $excel[$index][] = $nomor;
                    $nomor++;
                }
                else{
                    $excel[$index][] = $values;
                }
            }
            $index++;
        }
        $template = array(
            'table_open'=>'<table border="1" cellpadding="4" cellspacing="0">'
        );
        $this->table->set_template($template);
       
      
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="List Pemenang '.$values.'.xls"');
        header('Cache-Control: max-age=0');
        echo "<h1> List Pemenang $values </h1>";
        echo $this->table->generate($excel);
        //debugger($excel);
    }
    
    /*AJAX HANDLER*/
    public function get_hadiah()
    {
        $this->output->unset_template();
        if($this->input->is_ajax_request())
        {
            $data = $this->hadiah_m->order_by(array('hadiah_utama'=>'DESC','created_at'=>'DESC'))->get_many_by(array('status'=>0));
            echo json_encode($data);
        }
    }
    
    public function konfirmasi_txt($method='cek')
    {
        $this->output->unset_template();
        $this->load->helper('file');
        
        if($method == 'cek')
        {
            $string = read_file('./assets/konfirmasi.txt');
            echo $string;
        }
        else{
            $data = "true";
            write_file('./assets/konfirmasi.txt', $data, 'w');
        }
    }
    
    public function update_status_pemenang_hadiah_utama($status,$id_peserta,$id_hadiah)
    {
        $this->output->unset_template();

        //jika pemenang hadir maka qty di kurang 1
        if($status == 'true'){
            $this->hadiah_m->kurangin_hadiah($id_hadiah);
            $this->pemenang_m->update_by(array('peserta_id'=>$id_peserta),array('status'=>1));

            //cek apakah hadiah utama sudah habis? jika sudah,hide dari list hadiah / update statusnya.
            $cek = $this->hadiah_m->get($id_hadiah);
            $qty_hadiah_utama = $cek['qty'];
            if($qty_hadiah_utama == 0){
                $this->hadiah_m->update($id_hadiah,array('status'=>1));
                echo 'sukses update peserta dan hadiah';
            }
            else{
                echo 'update';
            }
        }else{
            $this->pemenang_m->delete_by(array('peserta_id'=>$id_peserta));
            echo 'delete';
        }
    }
    
    public function get_pemenang()
    {
        $this->output->unset_template();
        $qty = $this->input->post('qty');
        $hadiah_id = $this->input->post('hadiah_id');
        $kategori = $this->input->post('kategori');
        $explode = explode(",",$kategori);
        
        $data = $this->peserta_m->get_many_by(array('kategori'=>$explode,'status'=>false,'hadir'=>true));
        $hitung = count($data);

        //cek apakah hadiah utama atau bukan
        $cek = $this->hadiah_m->get(array('id'=>$hadiah_id));
        $qty_hadiah_utama = $cek['qty'];
        $hadiah_utama = ($cek['hadiah_utama'] == 1)?true:false;
        
        if($hitung >= $qty)
        {
            $get_offset = $hitung-$qty;
            $start = rand(0,$get_offset);
            $limit = $qty;
            
            $data = $this->peserta_m
                ->order_by('rand()')
                ->limit($limit,$start)
                ->get_many_by(array('kategori'=>$explode,'status'=>false,'hadir'=>true));
            
            foreach($data as $v)
            {
                if($hadiah_utama == true)
                {
                    $insert = array(
                        "peserta_id"=>$v['id'],
                        "hadiah_id"=>$hadiah_id,
                        "status"=>0
                    );
                }else{
                    $insert = array(
                        "peserta_id"=>$v['id'],
                        "hadiah_id"=>$hadiah_id,
                        "status"=>1
                    );
                }
                
                $this->peserta_m->update($v['id'],array('status'=>1));
                $this->pemenang_m->insert($insert);
            }

            //jika hadiah utama update hide hadiah di lakukan saat konfirmasi
            if($hadiah_utama == false)
            {
                $this->hadiah_m->update($hadiah_id,array('status'=>1));
            }
            echo "true";
        }
        else{
            echo "false";
        }
    }
    
    public function list_pemenang($hadiah_id=null,$tipe=null)
    {
        header('Content-type:application/json');
        
        $this->output->unset_template();

        $data = $this->pemenang_m->with('hadiah')->with('peserta')->get_many_by(array('hadiah_id'=>$hadiah_id));

        foreach($data as $v)
        {
            $json['title'] = $v->hadiah['nama_hadiah'];
            $json['data'][] = array(
                "nik"=>$v->peserta['nik'],
                "nama"=>$v->peserta['nama'],
                "hadiah"=>$v->hadiah['nama_hadiah'],
            );
        }
        
        echo json_encode($json);   
    }
    
    public function get_id_hadiah()
    {
        header('Content-type:application/json');
        $this->output->unset_template();
        $data = $this->hadiah_m->order_by('rand()')->limit(1)->get_by(array('status'=>1));
        echo json_encode(array('getId'=>$data['id'],"getHadiah"=>$data['nama_hadiah']));
    }
    
    public function board_pemenang($hadiah_id,$page=1){
        header('Content-type:application/json');
        
        $this->output->unset_template();
        define('PER_PAGE',12);
        
        $data = $this->pemenang_m->with('hadiah')->with('peserta')->get_many_by(array('hadiah_id'=>$hadiah_id));
        $hitung = count($data);
        $maxPage = ceil($hitung/PER_PAGE);
        
        if($page <= $maxPage)
        {
            if($page == 1)
            {
                $offset = 0; 
            }
            else{
                $offset = ($page-1) * PER_PAGE;
            }
        }
        else{
            $offset = ($maxPage-1) * PER_PAGE;
        }
        
        $paging = $this->pemenang_m->with('hadiah')->with('peserta')->limit(PER_PAGE,$offset)->get_many_by(array('hadiah_id'=>$hadiah_id));
        
        foreach($paging as $v)
        {
            $board[] = array(
                "sDate"=> "today",
                "sTime"=>$v->peserta['nik'],
                "sDeparture"=>$v->peserta['nama'],
                "nStatus"=> 1,
                "nTrack"=> 1,
                "fLight"=> $this->faker->boolean,
                "maxPage" => $maxPage
            );
        }
        
        if($this->input->get('max_page') == 'ok')
        {
            $max = array(
                "maxPage"=>$maxPage
            );
            echo json_encode($max);
        }
        else{
            echo json_encode($board);
        }
    }
    /*AJAX HANDLER*/
}
