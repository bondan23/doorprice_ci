<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}
	
	public function import()
	{
		$this->load->library('excel');
		
		$config['upload_path'] = './assets/';
		$config['allowed_types'] = 'xls|xlsx';
		$this->load->library('upload',$config);
		
		if($this->upload->do_upload())
		{
			
			$inputFileName = realpath("./assets/".$this->upload->file_name);
			
			$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFileName);
			
			foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
				$arrayData[$worksheet->getTitle()] = $worksheet->toArray();
			}
			
			$index = 0;

			foreach($arrayData['Sheet1'] as $key=>$v)
			{
				$kode = $v[0];
				$tahap_nama_kendali = $v[1];
				$tahap_satuan = $v[2];
				$tahap_volume = $v[3];
				$tahap_harga = str_replace(",","",$v[4]);
				$tahap_jumlah = str_replace(",","",$tahap_harga)*$tahap_volume;
				
				$data[]= array(
					"kode"=>$kode,
					"tahap_nama_kendali"=>$tahap_nama_kendali,
					"tahap_satuan"=> ($tahap_satuan == null)?'Ls':$tahap_satuan,
					"tahap_volume"=> ($tahap_volume == null)?'1':$tahap_volume,
					"tahap_harga" => ($tahap_harga == null)?'0':$tahap_harga,
					"tahap_jumlah" => $tahap_jumlah,
				);
			}
			
			foreach($data as $value)
			{
				$data= array(
					"kode"=>$value['kode'],
					"tahap_nama_kendali"=>$value['tahap_nama_kendali'],
					"tahap_satuan"=> $value['tahap_satuan'],
					"tahap_volume"=> $value['tahap_volume'],
					"tahap_harga" => $value['tahap_harga'],
					"tahap_jumlah" => $value['tahap_jumlah'],
				);
				
				$this->db->insert("input_kontrak",$data);
			}
			
			echo "OK";
			
		}
		else
		{
			echo  $this->upload->display_errors();
		}
		
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */