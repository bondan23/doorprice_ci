<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . "libraries/BasicAuth.php";

class Backend_Controller extends CI_Controller {

    private $model = [
        'hadiah_m',
        'peserta_m',
        'pemenang_m',
        'header_m',
        'kategori_m'
    ];
    
    public function __construct()
    {
        parent::__construct();
        $auth = new Authenticate\BasicAuth(array('admin' => 'admindoorprice'));
        
        // initiate faker
        $this->faker = Faker\Factory::create("id_ID");
        $this->_init();
        //init model
        $this->load->model($this->model);
    }
    
    protected function _init()
    {
        $this->output->set_template('system');
        $this->load->css('assets/css/style.css');
        $this->output->set_title('.: KEIHIN :.');
        //$this->load->section('modal', 'ci_simplicity/sidebar');
    }
    
    protected function _jqwidget()
    {
        //css
		$this->load->css('assets/jqwidgets/styles/jqx.base.css');
        
        //js core
		$this->load->js('assets/jqwidgets/jqxcore.js');
		$this->load->js('assets/jqwidgets/jqxdata.js');
		//js extra
		$this->load->js('assets/jqwidgets/jqxbuttons.js');
		$this->load->js('assets/jqwidgets/jqxscrollbar.js');
		$this->load->js('assets/jqwidgets/jqxmenu.js');
		$this->load->js('assets/jqwidgets/jqxlistbox.js');
		$this->load->js('assets/jqwidgets/jqxcheckbox.js');
		$this->load->js('assets/jqwidgets/jqxradiobutton.js');
		$this->load->js('assets/jqwidgets/jqxdropdownlist.js');
		$this->load->js('assets/jqwidgets/jqxgrid.js');
		$this->load->js('assets/jqwidgets/jqxinput.js');
		$this->load->js('assets/jqwidgets/jqxwindow.js');
		$this->load->js('assets/jqwidgets/jqxgrid.sort.js');
		$this->load->js('assets/jqwidgets/jqxgrid.pager.js');
		$this->load->js('assets/jqwidgets/jqxgrid.edit.js');
		$this->load->js('assets/jqwidgets/jqxgrid.selection.js');
		$this->load->js('assets/jqwidgets/jqxgrid.columnsresize.js');
		$this->load->js('assets/jqwidgets/jqxdata.export.js');
		$this->load->js('assets/jqwidgets/jqxgrid.export.js');
    }
    
}