<?php

//Auth 3 - HTTP Basic Auth in OOP with infinite users attempts per browsing session.
namespace Authenticate;
class BasicAuth {
	//we hardcode users and passwords in an array
	//we can get them from a database instead
	private $users = array();
	public function __construct($users = array("courage" => "dog", "girls" => "powerpuff", "eric" => "cartman")) {
		$this->users = $users;
		$this->authenticate();
	}
	
	private function authenticate() {
		if (!isset($_SERVER['PHP_AUTH_USER']) || isset($_SESSION['logout'])) {
            session_destroy();
			$this->setAuthHeaders();
			//logic to send if the users cancels authentication
			echo "Credentials are required to access the webpage!";
			exit;
		} else {
			if (!array_key_exists($_SERVER["PHP_AUTH_USER"], $this->users) || $this->users[$_SERVER["PHP_AUTH_USER"]] !== $_SERVER['PHP_AUTH_PW']) {
                $this->setAuthHeaders();
				echo "Refresh to try login back!";
				exit;
			}
            else{
                unset($_SESSION['logout']);
                $_SESSION['login'] = true;
            }
		}
	}
	 
	private function setAuthHeaders() {
		// send headers for the browser to show an authentication prompt box
		header('WWW-Authenticate: Basic realm="My Website Auth"');
		header('HTTP/1.0 401 Unauthorized');
	}
}