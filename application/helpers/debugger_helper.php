<?php

if(!function_exists('debugger'))
{
    function debugger($data)
    {
        echo "<pre>";
        print_r($data);
        echo "<pre>";
    }
}