<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
   
    
    private $model = [
        'hadiah_m',
        'peserta_m',
        'pemenang_m',
        'header_m'
    ];
    
    public function __construct()
    {
        parent::__construct();
        // initiate faker
        $this->faker = Faker\Factory::create("id_ID");
        $this->_init();
        
        //init model
        $this->load->model($this->model);
        $this->_header();
    }
    
    protected function _init()
    {
        $this->output->set_template('default');
        $this->output->set_title('.: KEIHIN :.');
        //$this->load->section('modal', 'ci_simplicity/sidebar');
    }
    
    private function _header()
    {
        $header = $this->header_m->get_all();
        foreach($header as $v){
            $data[$v['tipe']] = $v['text'];
        }
        
        $this->output->set_header_aplikasi($data['header']);
        $this->output->set_tajuk_acara($data['tajuk']);
    }
    
}