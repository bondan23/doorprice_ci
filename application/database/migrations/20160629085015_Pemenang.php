<?php

class Migration_Pemenang extends CI_Migration {

    public function up() {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'peserta_id'=> array(
                'type' => 'INT',
                'constraint' => 5
            ),
            'hadiah_id'=>array(
                'type' => 'INT',
                'constraint' => 5
            ),
            'status'=>array(
                'type'=> 'TINYINT',
                'constraint'=>1
            ),
            'created_at'=>array(
                'type' => 'DATETIME'
            ),
            'updated_at'=>array(
                'type' => 'DATETIME'
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('pemenang');
    }

    public function down() {
        $this->dbforge->drop_table('pemenang');
    }

}