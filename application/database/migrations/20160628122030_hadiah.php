<?php

class Migration_hadiah extends CI_Migration {

    public function up() {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'hadiah_utama'=>array(
                'type'=> 'BOOLEAN',
                'default'=>false,
            ),
            'nama_hadiah'=>array(
                'type'=> 'VARCHAR',
                'constraint'=>100
            ),
            'qty'=>array(
                'type'=> 'INT',
                'constraint' => 3
            ),
            'kategori'=>array(
                'type'=> 'VARCHAR',
                'constraint' => 10
            ),
            'images'=>array(
                'type'=> 'VARCHAR',
                'constraint' => 150
            ),
            'status'=>array(
                'type'=> 'TINYINT',
                'constraint'=>1
            ),
            'created_at'=>array(
                'type'=> 'DATETIME'
            ),
            'updated_at'=>array(
                'type'=> 'DATETIME'
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('hadiah');
    }

    public function down() {
        $this->dbforge->drop_table('hadiah');
    }

}