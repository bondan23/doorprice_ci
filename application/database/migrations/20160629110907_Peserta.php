<?php

class Migration_Peserta extends CI_Migration {

    public function up() {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'nik'=>array(
                'type' => 'VARCHAR',
                'constraint' => 20
            ),
            'nama'=>array(
                'type' => 'VARCHAR',
                'constraint' => 100
            ),
            'kategori'=>array(
                'type'=> 'VARCHAR',
                'constraint' => 10
            ),
            'department'=>array(
                'type'=> 'VARCHAR',
                'constraint' => 50
            ),
            'status'=>array(
                'type'=> 'TINYINT',
                'constraint'=>1
            ),
            'hadir'=>array(
                'type'=> 'TINYINT',
                'constraint'=>1
            ),
            'created_at'=>array(
                'type' => 'DATETIME'
            ),
            'updated_at'=>array(
                'type' => 'DATETIME'
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('peserta');
    }

    public function down() {
        $this->dbforge->drop_table('peserta');
    }

}