<?php

class Migration_kategori extends CI_Migration {

    public function up() {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'kategori' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'unique' => TRUE
            ),
            'created_at'=>array(
                'type'=> 'DATETIME'
            ),
            'updated_at'=>array(
                'type'=> 'DATETIME'
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('kategori');
    }

    public function down() {
        $this->dbforge->drop_table('kategori');
    }

}