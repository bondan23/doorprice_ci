<?php

class Migration_header extends CI_Migration {

    public function up() {
        $this->dbforge->add_field(array(
            'tipe'=> array(
                'type'=> 'VARCHAR',
                'constraint' => '100'
            ),
            'text'=> array(
                'type' => 'TEXT'
            ),
            'created_at'=>array(
                'type' => 'DATETIME'
            ),
            'updated_at'=>array(
                'type' => 'DATETIME'
            )
        ));
        $this->dbforge->add_key('tipe', TRUE);
        $this->dbforge->create_table('header');
    }

    public function down() {
        $this->dbforge->drop_table('header');
    }

}