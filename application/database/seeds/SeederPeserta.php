<?php

class SeederPeserta extends Seeder {

    private $table = 'peserta';

    public function run() {
        $this->db->truncate($this->table);
        
        //seed records manually
        
        $limit = 100;
        echo "seeding $limit user accounts";

        for ($i = 0; $i < $limit; $i++) {
            echo ".";

            $data = array(
                'nik' => "KID".$this->faker->unique()->randomNumber(7),
                'nama' => $this->faker->unique()->name,
                'kategori'=>$this->faker->randomElement(range("A","D")),
                "created_at"=> date('Y-m-d H:i:s'),
                "updated_at"=> date('Y-m-d H:i:s')
            );

            $this->db->insert($this->table, $data);
        }

        echo PHP_EOL;
    }
}
