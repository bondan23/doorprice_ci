<?php

class SeederHadiah extends Seeder {

    private $table = 'hadiah';

    public function run() {
        $this->db->truncate($this->table);

        //seed records manually
        $data = [
            [
                "nama_hadiah"=>"Mobil",
                "qty"=>$this->faker->numberBetween($min = 1, $max = 1),
                "kategori"=>$this->faker->randomElement(range("A","D")),
                "hadiah_utama"=>1,
                "images"=>$this->faker->unique()->imageUrl(400, 300, 'abstract')
            ],
            [
                "nama_hadiah"=>"Motor",
                "qty"=>$this->faker->numberBetween($min = 1, $max = 3),
                "kategori"=>$this->faker->randomElement(range("A","D")),
                "hadiah_utama"=>0,
                "images"=>$this->faker->unique()->imageUrl(400, 300, 'abstract')
            ],
            [
                "nama_hadiah"=>"Laptop",
                "qty"=>$this->faker->numberBetween($min = 2, $max = 4),
                "kategori"=>$this->faker->randomElement(range("A","D")),
                "hadiah_utama"=>0,
                "images"=>$this->faker->unique()->imageUrl(400, 300, 'abstract')
            ],
            [
                "nama_hadiah"=>"Ricecooker",
                "qty"=>$this->faker->numberBetween($min = 5, $max = 10),
                "kategori"=>$this->faker->randomElement(range("A","D")),
                "hadiah_utama"=>0,
                "images"=>$this->faker->unique()->imageUrl(400, 300, 'abstract')
            ],
            [
                "nama_hadiah"=>"Kipas Angin",
                "qty"=>$this->faker->numberBetween($min = 10, $max = 20),
                "kategori"=>$this->faker->randomElement(range("A","D")),
                "hadiah_utama"=>0,
                "images"=>$this->faker->unique()->imageUrl(400, 300, 'abstract')
            ],
        ];
        
        echo "seeding data";
        
        foreach($data as $v)
        {
            echo ".";
            
            $insert = array(
                "nama_hadiah"=> $v['nama_hadiah'],
                "qty"=> $v['qty'],
                "kategori"=> $v['kategori'],
                "images"=> $v['images'],
                "hadiah_utama" => $v['hadiah_utama']
            );
                
            $this->db->insert($this->table, $insert);
        }

        echo PHP_EOL;
    }
}
