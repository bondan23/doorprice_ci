<?php

class SeederHeader extends Seeder {

    private $table = 'header';

    public function run() {
        $this->db->truncate($this->table);

        $data = array(
           array(
             'tipe' => 'header',
             'text' => 'Keihin Indonesia',
             'created_at'=>date('Y-m-d H:i:s'),
             'updated_at'=>date('Y-m-d H:i:s')
           ),
           array(
             'tipe' => 'tajuk',
             'text' => 'Event Pengundian Doorprize',
             'created_at'=>date('Y-m-d H:i:s'),
             'updated_at'=>date('Y-m-d H:i:s')
           )
        );

        echo "seeding data";
        
        foreach($data as $v)
        {
            echo ".";
            
            $insert = array(
                "tipe"=> $v['tipe'],
                "text"=> $v['text'],
                "created_at"=> $v['created_at'],
                "updated_at"=> $v['updated_at'],
            );
                
            $this->db->insert($this->table, $insert);
        }

        echo PHP_EOL;
    }
}
