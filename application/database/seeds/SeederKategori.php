<?php

class SeederKategori extends Seeder {

    private $table = 'kategori';

    public function run() {
        $this->db->truncate($this->table);

        //seed records manually
        
        $data = range("A","J");

        echo "seeding kategori";

        foreach($data as $v){
            echo ".";
            $insert = array(
                "kategori"=>$v
            );
            $this->db->insert($this->table, $insert);
        }

        echo PHP_EOL;
    }
}
