<?php
        
class Peserta_m extends MY_Model
{
    protected $_table = 'peserta';
    protected $primary_key = 'id';
    protected $return_type = 'array';
    
    /**
     * Support for soft deletes and this model's 'deleted' key
     */
    protected $soft_delete = FALSE;
    protected $soft_delete_key = 'deleted'; // if soft_delete == true | harus ada field deleted di database
    protected $_temporary_with_deleted = FALSE; // keluarkan semua data meskipun statusnya soft deleted
    protected $_temporary_only_deleted = FALSE; // keluarkan data yang hanya sudah di softdelete
    
    /**
     * The various callbacks available to the model. Each are
     * simple lists of method names (methods will be run on $this).
     */
    protected $before_create = array('created_at','updated_at');
    protected $after_create = array();
    protected $before_update = array('updated_at');
    protected $after_update = array();
    protected $before_get = array();
    protected $after_get = array();
    protected $before_delete = array();
    protected $after_delete = array();
    
    
    public function search($q)
    {
        $this->db->select('*');
        $this->db->from($this->_table);
        $this->db->like('LOWER(nama)',strtolower($q));
        $this->db->or_like('LOWER(nik)',strtolower($q));
        $query = $this->db->get();
        return $query->result_array();
    }
}
        