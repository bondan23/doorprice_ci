<!DOCTYPE html>
<html lang="en">
    <head>
        <title>
            <?php echo $title; ?>
        </title>
        <link rel=icon href=<?php echo base_url() ?>assets/favicon.ico sizes="16x16" type="image/png">
        <?php
            if(!empty($meta)) 
                foreach($meta as $name=>$content){
                    echo "\n\t\t"; 
                    ?><meta name="<?php echo $name; ?>" content="<?php echo is_array($content) ? implode(", ", $content) : $content; ?>" /><?php
             }
        ?>

        <!-- DEFAULT CSS  -->
        <link href="<?php echo base_url() ?>assets/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
        <link href="<?php echo base_url() ?>assets/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>

        <?php
                if(!empty($css)) 
                 foreach($css as $file){ 
                    echo "\n\t\t"; 
                    ?><link rel="stylesheet" href="<?php echo $file; ?>" type="text/css" /><?php
                 } echo "\n\t"; 
        ?>

        <script>
          window.odometerOptions = {
            format: '', // Change how digit groups are formatted, and how many digits are shown after the decimal point
            duration: 3000, // Change how long the javascript expects the CSS animation to take
          };
        </script>
    </head>
    <body>
        <?php 
        
        if($this->load->get_section('loader')):
            echo $this->load->get_section('loader');
        endif; 
        
        if($this->load->get_section('modal')):
            echo $this->load->get_section('modal');
        endif;
        ?>

        <nav class="light-blue lighten-1" role="navigation">
            <div class="nav-wrapper container">
                <a href="<?php echo base_url(); ?>" class="brand-logo center white-text" style="font-size:2.2em;"><?php echo $header; ?></a>
                <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
            </div>
        </nav>
        <div class="container light-blue lighten-1" style="margin-top:10px;border-radius:20px;">
            <div class="row center-align">
                <h1 style="margin:0px;padding:10px;font-family:'test';font-size:3.5em" class="white-text text-lighten-1"><?php echo $tajuk_acara ?></h1>
            </div>
        </div>
        <?php echo $output;?>
        
        
        <footer class="page-footer" style="margin-top:0px;">
            <div class="footer-copyright">
                <div class="container">
                    Copyright &copy; 2016 PT.Keihin Indonesia
                </div>
            </div>
        </footer>

        <!--  Scripts-->
        <script type="text/javascript">
            var link = "<?php echo base_url() ?>";
        </script>
        <script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/materialize.js"></script>
    <?php
             foreach($js as $file){
                    echo "\n\t\t"; 
                    ?><script src="<?php echo $file; ?>"></script><?php
             } echo "\n\t"; 
    ?>
    </body>
</html>