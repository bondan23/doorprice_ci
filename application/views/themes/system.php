<!DOCTYPE html>
<html lang="en">
    <head>
        <title>
            <?php echo $title; ?>
        </title>
        <?php
            if(!empty($meta)) 
                foreach($meta as $name=>$content){
                    echo "\n\t\t"; 
                    ?><meta name="<?php echo $name; ?>" content="<?php echo is_array($content) ? implode(", ", $content) : $content; ?>" /><?php
             }
        ?>

        <!-- DEFAULT CSS  -->
        <link href="<?php echo base_url() ?>assets/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
        <link href="<?php echo base_url() ?>assets/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
        <link href="<?php echo base_url() ?>assets/jquery-editable/css/jquery-editable.css" type="text/css" rel="stylesheet"/>
        <link href="<?php echo base_url() ?>assets/jquery-editable/tip-yellow/tip-yellow.css" type="text/css" rel="stylesheet"/>
        
        <?php
                if(!empty($css)) 
                 foreach($css as $file){ 
                    echo "\n\t\t"; 
                    ?><link rel="stylesheet" href="<?php echo $file; ?>" type="text/css" /><?php
                 } echo "\n\t"; 
        ?>
    </head>
    <body>
        <!-- Dropdown Structure -->
        <ul id="dropdown1" class="dropdown-content">
          <li><a href="<?php echo site_url('secure/logout') ?>">Logout</a></li>
        </ul>
       
        <nav class="grey darken-1" role="navigation">
            <div class="nav-wrapper had-container">
                <a href="<?php echo site_url('secure/dashboard'); ?>" class="brand-logo center">Admin Page</a>
                <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
                <ul class="right hide-on-med-and-down">
                  <!-- Dropdown Trigger -->
                  <li><a class="dropdown-button" href="#!" data-activates="dropdown1">Welcome, Admin<i class="material-icons right">arrow_drop_down</i></a></li>
                </ul>
            </div>
        </nav>

        <?php echo $output;?>

        <?php 
        
        if($this->load->get_section('modal')):
            echo $this->load->get_section('modal');
        endif; 
        
        ?>
        
        <!--  Scripts-->
        <script type="text/javascript">
            var link = "<?php echo base_url() ?>";
        </script>
        <script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/materialize.js"></script>
        <script src="<?php echo base_url() ?>assets/jquery-editable/js/jquery.poshytip.min.js"></script>
        <script src="<?php echo base_url() ?>assets/jquery-editable/js/jquery-editable-poshytip.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/jquery.scannerdetection.js"></script>
        <?php
             foreach($js as $file){
                    echo "\n\t\t"; 
                    ?><script src="<?php echo $file; ?>"></script><?php
             } echo "\n\t"; 
        ?>
        <script type="text/javascript">
        /*SCANNER*/
        $(document).scannerDetection({
            timeBeforeScanTest: 200, // wait for the next character for upto 200ms
            startChar: [/*47*/], // Prefix character for the cabled scanner (OPL6845R)
            endChar: [/*47,*/13], // be sure the scan is complete if key 13 (enter) is detected
            avgTimeByChar: 40, // it's not a barcode if a character takes longer than 40ms
            onComplete: function(barcode, qty){ 
                /*$('.barcode_check').focus();
                $('.barcode_check').val(barcode);*/
                var barcode = barcode.replace(/\s/g,'');
                    
                $.getJSON(link+'secure/checkin/check/'+barcode,function(a){
                   if(a.status == true)
                   {
                       console.log('Sukses Checkin');
                       $('.barcode_check').val('');
                       $(".data_peserta_hadir").jqxGrid("updatebounddata");
                   }
                   else{
                       $('.barcode_check').val('');
                   }
               })
            } // main callback function	
        });
        /*SCANNER*/
        </script>
    </body>
</html>