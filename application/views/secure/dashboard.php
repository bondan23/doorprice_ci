<div class="container">
    <div class="row">
        <h3 class="center-align"> <b class="blue-text">Keihin Indonesia</b> - Doorprice Application</h3>
        <?php if($this->session->flashdata('success')){ ?>
            <h5 class="red-text text-lighten-1 center-align"><?php echo $this->session->flashdata('success'); ?></h5>
        <?php } ?>
        
        
        <div class="col s12 m6">
            <a href="<?php echo site_url('secure/header') ?>">
                <div class="card blue-grey darken-1">
                    <div class="card-content white-text">
                        <div class="card-title center-align">Header &amp; Tajuk Acara</div>
                        <div class="center-align">
                            <i class="material-icons" style="font-size:10em">subject</i>
                        </div>
                    </div>
                </div>
            </a> 
        </div>
        
        
        <?php if($hitung_peserta > 0){ ?>
        <div class="col s12 m6">
            <a href="<?php echo site_url('secure/import/data_peserta') ?>">
                <div class="card blue-grey darken-1">
                    <div class="card-content white-text">
                        <div class="card-title center-align">Data Peserta</div>
                        <div class="center-align">
                            <i class="material-icons" style="font-size:10em">perm_identity</i>
                        </div>
                    </div>
                </div>
            </a> 
        </div>
        <?php } else { ?>
        
        <div class="col s12 m6">
            <a href="<?php echo site_url('secure/import') ?>">
                <div class="card blue-grey darken-1">
                    <div class="card-content white-text">
                        <div class="card-title center-align">Import Peserta</div>
                        <div class="center-align">
                            <i class="material-icons" style="font-size:10em">perm_identity</i>
                        </div>
                    </div>
                </div>
            </a> 
        </div>
        <?php } ?>
        
        <div class="col s12 m6">
            <a href="<?php echo site_url('secure/kategori') ?>">
                <div class="card blue-grey darken-1">
                    <div class="card-content white-text">
                        <div class="card-title center-align">Input Kategori Hadiah</div>
                        <div class="center-align">
                            <i class="material-icons" style="font-size:10em">shopping_cart</i>
                        </div>
                    </div>
                </div>
            </a> 
        </div>
        
        <div class="col s12 m6">
            <a href="<?php echo site_url('secure/hadiah') ?>">
                <div class="card blue-grey darken-1">
                    <div class="card-content white-text">
                        <div class="card-title center-align">Input Hadiah</div>
                        <div class="center-align">
                            <i class="material-icons" style="font-size:10em">shopping_cart</i>
                        </div>
                    </div>
                </div>
            </a> 
        </div>
        

        <div class="col s12 m6">
            <a href="<?php echo site_url('secure/download') ?>">
                <div class="card blue-grey darken-1">
                    <div class="card-content white-text">
                        <div class="card-title center-align">Download Template Import</div>
                        <div class="center-align">
                            <i class="material-icons" style="font-size:10em">system_update_alt</i>
                        </div>
                    </div>
                </div>
            </a> 
        </div>
        
        <div class="col s12 m6">
            <a href="<?php echo site_url('secure/backup') ?>">
                <div class="card blue-grey darken-1">
                    <div class="card-content white-text">
                        <div class="card-title center-align">Backup Database</div>
                        <div class="center-align">
                            <i class="material-icons" style="font-size:10em">system_update_alt</i>
                        </div>
                    </div>
                </div>
            </a> 
        </div>
        
        
        <div class="col s12 m6">
            <a href="<?php echo site_url('secure/checkin'); ?>">
                <div class="card blue-grey darken-1">
                    <div class="card-content white-text">
                        <div class="card-title center-align">Check In Barcode</div>
                        <div class="center-align">
                            <i class="material-icons" style="font-size:10em">verified_user</i>
                        </div>
                    </div>
                </div>
            </a> 
        </div>
        <div class="col s12 m6">
            <a href="<?php echo site_url('secure/reset'); ?>">
                <div class="card blue-grey darken-1">
                    <div class="card-content white-text">
                        <div class="card-title center-align">Reset Application</div>
                        <div class="center-align">
                            <i class="material-icons" style="font-size:10em">loop</i>
                        </div>
                    </div>
                </div>
            </a> 
        </div>
    </div>
</div>