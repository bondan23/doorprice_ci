<div class="had-container">
    <div class="row">
       <div class="col l12">
           <div style="margin-top:2em;" class="card panel">
                <div class="card-header grey darken-1 panel">
                    <h5 class="center-align panel-text">Header</h5>
                </div>
                <div class="card-content center-align">
                    <a href="javascript:void(0)" id="header" data-pk="header" style="font-size:3em"><?php echo $header ?></a>
               </div>
            </div>
        </div>
       <div class="col l12">
           <div style="margin-top:2em;" class="card panel">
                <div class="card-header grey darken-1 panel">
                    <h5 class="center-align panel-text">Tajuk Acara</h5>
                </div>
                <div class="card-content center-align">
                    <a href="javascript:void(0)" id="tajuk" data-pk="tajuk" style="font-size:3em"><?php echo $tajuk ?></a>
               </div>
            </div>
        </div>
    </div>
</div>