<div class="container">
    <div class="row">
       <div style="margin-top:2em;" class="card panel">
            <div class="card-header grey darken-1 panel">
                <h5 class="center-align panel-text">Data Hadiah</h5>
            </div>
            <div class="card-content">
                <div class="data_hadiah"></div>
                <div id="jqxwindow" style="display:none;">
                    <div id="header-form">A</div>
                    <div id="isi">
                        <div class="row">
                            <form id="form_hadiah" class="col s12" enctype="multipart/form-data" method="POST">
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="nama_hadiah" name="nama_hadiah" type="text" class="validate" required>
                                        <label for="nama_hadiah">Nama Hadiah</label>
                                    </div>
                                    <div class="input-field col s2">
                                        <input id="qty" type="text" name="qty" class="validate center-align" required>
                                        <label for="qty">QTY</label>
                                    </div>
                                    <div class="col s12">
                                        <label>Kategori</label>
                                        <p>
                                            <?php 
                                                foreach($kategori as $v){
                                            ?>
                                                <input type="checkbox" id="<?php echo $v['kategori'] ?>" value="<?php echo strtoupper($v['kategori']) ?>" name="kategori[]"/>
                                                <label for="<?php echo $v['kategori'] ?>" class="checkbox"><?php echo $v['kategori'] ?></label>
                                            <?php
                                                }
                                            ?>
                                        </p>
                                        <!--<label>Kategori</label>
                                        <select class="select_kategori" name="kategori">
                                            <option value="" disabled selected>Pilih Kategori</option>
                                            <option value="A">A</option>
                                            <option value="B">B</option>
                                            <option value="C">C</option>
                                            <option value="D">D</option>
                                            <option value="E">E</option>
                                        </select>-->
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="image-editor file-field input-field">
                                       <div class="btn">
                                            <span>Photo</span>
                                            <input type="file" name="userfile" class="cropit-image-input">
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text" placeholder="Pilih Photo Hadiah">
                                        </div>
                                        <div class="cropit-preview"></div>
                                        <div class="image-size-label">
                                        Resize image
                                        </div>
                                        <p class="range-field">
                                            <input type="range" class="cropit-image-zoom-input" min="0" max="100">
                                        </p>
                                        <input type="hidden" name="image-data" class="hidden-image-data" />
                                    </div>
                                    <div class="progress">
                                        <div class="determinate" style="width:0%"></div>
                                    </div>
                                    <!--<div class="file-field input-field">
                                        <div class="btn">
                                            <span>Photo</span>
                                            <input type="file" name="userfile">
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text" placeholder="Pilih Photo Hadiah">
                                        </div>
                                    </div>
                                     <div class="progress">
                                          <div class="determinate" style="width:0%"></div>
                                     </div>-->
                                </div>
                                <div class="row">
                                    <div class="col offset-s4 s5">
                                        <input type="hidden" name="method" />
                                        <button class="waves-effect waves-light btn red darken-1" type="button" id="cancel">Cancel</button>
                                        <button class="waves-effect waves-light btn" type="submit">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
           </div>
        </div>
    </div>
</div>