<div class="container">
    <div class="row">
       <div style="margin-top:2em;" class="card panel">
            <div class="card-header grey darken-1 panel">
                <h5 class="center-align panel-text">Reset Menu</h5>
            </div>
            <div class="card-content">
                <div class="row">
                   <?php if($hadiah != null): ?>
                    <div class="col s12 m4">
                        <a href="javascript:void(0)" onclick="prompt('hadiah')">
                            <div class="card blue-grey darken-1">
                                <div class="card-content white-text">
                                    <div class="card-title center-align">Reset Hadiah</div>
                                    <div class="center-align">
                                        <i style="font-size:10em" class="material-icons">loop</i>
                                    </div>
                                </div>
                            </div>
                        </a> 
                    </div>
                    <?php endif; ?>
                    
                    <?php if($peserta != null): ?>
                    <div class="col s12 m4">
                        <a href="javascript:void(0)" onclick="prompt('peserta')">
                            <div class="card blue-grey darken-1">
                                <div class="card-content white-text">
                                    <div class="card-title center-align">Reset Peserta</div>
                                    <div class="center-align">
                                        <i style="font-size:10em" class="material-icons">loop</i>
                                    </div>
                                </div>
                            </div>
                        </a> 
                    </div>
                    <?php endif; ?>
                    
                    <?php if($peserta == null && $hadiah == null): ?>
                    <div class="col s12 push-m4 m4">
                    <?php else:?>
                    <div class="col s12 m4">
                    <?php endif; ?>
                        <a href="javascript:void(0)" onclick="prompt('all')">
                            <div class="card blue-grey darken-1">
                                <div class="card-content white-text">
                                    <div class="card-title center-align">Reset All Content</div>
                                    <div class="center-align">
                                        <i style="font-size:10em" class="material-icons">loop</i>
                                    </div>
                                </div>
                            </div>
                        </a> 
                    </div>
                </div>
           </div>
        </div>
    </div>
</div>