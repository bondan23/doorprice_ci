<div class="container">
    <div class="row">
       <div style="margin-top:2em;" class="card panel">
            <div class="card-header grey darken-1 panel">
                <h5 class="center-align panel-text">Data Kategori Hadiah</h5>
            </div>
            <div class="card-content">
                <div class="data_kategori" style="margin:0 auto;"></div>
                <div id="jqxwindow" style="display:none;">
                    <div id="header-form">A</div>
                    <div id="isi">
                        <div class="row">
                            <form id="form_kategori" class="col s12" enctype="multipart/form-data" method="POST">
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="nama_kategori" name="nama_kategori" type="text" class="validate" required>
                                        <label for="nama_kategori">Kategori</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col offset-s4 s5">
                                        <input type="hidden" name="method" />
                                        <button class="waves-effect waves-light btn red darken-1" type="button" id="cancel">Cancel</button>
                                        <button class="waves-effect waves-light btn" type="submit">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
           </div>
        </div>
    </div>
</div>