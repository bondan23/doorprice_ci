<div class="container">
    <div class="row">
       <div style="margin-top:2em;" class="card panel">
            <div class="card-header grey darken-1 panel">
                <h5 class="center-align panel-text">Data Peserta Hadir</h5>
            </div>
            <div class="card-content">
                <div class="row">
                    <div class="input-field col s6 offset-s3">
                        <input id="barcode" type="text" class="validate barcode_check" placeholder="Scan / Type Barcode Here">
                        <label class="active" for="barcode">Barcode</label>
                    </div>
                </div>
                <div class="data_peserta_hadir"></div>
            </div>
        </div>
    </div>
</div>