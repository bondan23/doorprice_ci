<div class="container">
    <div class="row">
        <div style="margin-top:2em;" class="card panel">
            <div class="card-header grey darken-1 panel">
                <h5 class="center-align panel-text">Import Peserta Undian</h5>
            </div>
            <div class="card-content">
                <form role="form" method="POST" enctype="multipart/form-data" action="<?php echo site_url('secure/import/send') ?>">
                   <div class="row">
                   <div class="col s12">
                   <div class="row">
                    <div class="input-field col s6">
                      <input placeholder="Ex:Worksheet"  name="worksheet" id="worksheet" type="text" class="validate" required>
                      <label for="worksheet">WorkSheet Name</label>
                    </div>
                   </div>
                   <div class="row">
                    <div class="file-field input-field col s12">
                        <div class="btn">
                            <span>File</span>
                            <input type="file" multiple name="userfile">
                        </div>
                        <div class="file-path-wrapper">
                            <input class="file-path validate" type="text" placeholder="Upload one or more files">
                        </div>
                    </div>
                       </div>

                    <button type="submit" class="btn waves-effect waves-light">submit</button>
                    </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>