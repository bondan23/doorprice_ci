<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>KEIHIN WINNER BOARD</title>

    <!-- Fonts -->
   <!-- <link href="https://fonts.googleapis.com/css?family=Kelly+Slab" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz" rel="stylesheet" type="text/css">-->

    <!-- jQuery, transit (for animations), date.js and the solari board -->
    <script>
        var link = "<?php echo base_url() ?>"
    </script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery.transit.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/date.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/solari.js"></script>

    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/solari.css" />

    <!-- Audio -->
    <audio src="<?php echo base_url() ?>assets/audio/solari.mp3" id="solari-audio">
                Your browser does not support the audio element.
    </audio>
    <script>
		var idlink = link+'welcome/get_id_hadiah';
		var id = 1;
		var hadiah = '';
		
		window.setInterval(function (){
		
			$.getJSON(idlink,function(data){
				id = data.getId;
				hadiah = data.getHadiah;
				addSolariBoard();
			});
	
		}, 500 * REFRESH_TIME);
	
		$.getJSON(idlink,function(data){
			id = data.getId;
			hadiah = data.getHadiah;
			addSolariBoard();
		});
    </script>
</head>
</html>
