<div id="modal_hadiah" class="modal">
    <div class="modal-content">
        <h4>Verifikasi Pemenang</h4>
        <p>Apakah Pemenang Hadir ???</p>
    </div>
    <div class="modal-footer">
        <a class="modal-action waves-effect waves-white btn-flat green lighten-1 white-text" href="#!" onclick="konfirmasi_pemenang(this,true)">Iya</a>
        <a href="#!" class="konfirm modal-action waves-effect waves-white btn-flat red lighten-1 white-text" onclick="konfirmasi_pemenang(this,false)" data-id_hadiah="<?php echo $hadiah_id ?>">Tidak</a>
    </div>
</div>