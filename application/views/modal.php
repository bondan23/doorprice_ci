<div id="modal1" class="modal">
    <div class="modal-content">
      <h4>Verifikasi Reset</h4>
      <p>Anda yakin ingin me-reset ????</p>
    </div>
    <div class="modal-footer">
      <a href="#!" class="konfirm modal-action waves-effect waves-white btn-flat green lighten-1 white-text" onclick="konfirmasi(this)">Iya</a>
      <a class="modal-action modal-close waves-effect waves-white btn-flat red lighten-1 white-text" href="#!">Tidak</a>
    </div>
  </div>