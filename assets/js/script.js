var data = [
	{
		"id": 1,
		"nama": "Noble Nelson",
		"hadiah": "Laptop Asus"
	},
	{
		"id": 2,
		"nama": "Christine Salazar",
		"hadiah": "Laptop Asus"
	},
	{
		"id": 3,
		"nama": "Rigel K. Dalton",
		"hadiah": "Laptop Asus"
	},
	{
		"id": 4,
		"nama": "Phillip T. Frazier",
		"hadiah": "Laptop Asus"
	},
	{
		"id": 5,
		"nama": "Jasmine Wil˝ox",
		"hadiah": "Laptop Asus"
	},
	{
		"id": 6,
		"nama": "Acton D. Lambert",
		"hadiah": "Laptop Asus"
	},
	{
		"id": 7,
		"nama": "Kyla Gardner",
		"hadiah": "Laptop Asus"
	},
	{
		"id": 8,
		"nama": "Kevyn C. Hester",
		"hadiah": "Laptop Asus"
	},
	{
		"id": 9,
		"nama": "Ishmael I. Hayes",
		"hadiah": "Laptop Asus"
	},
	{
		"id": 10,
		"nama": "Levi Mullins",
		"hadiah": "Laptop Asus"
	},
	{
		"id": 11,
		"nama": "Quentin M. Mendez",
		"hadiah": "Laptop Asus"
	},
	{
		"id": 12,
		"nama": "Lydia Lewis",
		"hadiah": "Laptop Asus"
	},
	{
		"id": 13,
		"nama": "Elliott Kerr",
		"hadiah": "Laptop Asus"
	},
	{
		"id": 14,
		"nama": "Ciaran Shelton",
		"hadiah": "Laptop Asus"
	},
	{
		"id": 15,
		"nama": "Samson R. Scott",
		"hadiah": "Laptop Asus"
	},
	{
		"id": 16,
		"nama": "Wesley T. Bird",
		"hadiah": "Laptop Asus"
	},
	{
		"id": 17,
		"nama": "Jackson M. Charles",
		"hadiah": "Laptop Asus"
	},
	{
		"id": 18,
		"nama": "Scarlett A. Glenn",
		"hadiah": "Laptop Asus"
	},
	{
		"id": 19,
		"nama": "Garth Barber",
		"hadiah": "Laptop Asus"
	},
	{
		"id": 20,
		"nama": "Yasir Lowery",
		"hadiah": "Laptop Asus"
	},
];

/*var source =
{
    localdata: data,
    datatype: "array"
};*/

var pathArray = window.location.pathname;
var URIid = pathArray.match(/[0-9]+/);
var source =
{
    datatype: 'json',
    datafields: [
        {name: 'nik', type:'string'},
        {name: 'nama', type:'string'},
        {name: 'hadiah', type:'string'},
    ],
    root:'data',
    id: 'id',
    url: link+'welcome/list_pemenang/'+URIid[0]
};

var dataAdapter = new $.jqx.dataAdapter(source, {
    loadComplete: function (data) { },
    loadError: function (xhr, status, error) { }    
});


$(".list_pemenang").jqxGrid(
{
    width:'100%',
    source:dataAdapter,
    columnsresize: true,
    showtoolbar: true,
    autoheight: true,
    sortable: false,
    pageable: true,
    renderToolbar : function(toolbar)
    {
        var title = dataAdapter.loadedData.title;
        var container = $('<div style="margin-top:3px;overflow:hidden;"></div>');
        toolbar.append(container);
        container.append('<div style="width:13%;float:left;"><input id="exportButton" type="button" value="Export to Excel" /></div>');
        container.append('<div style="width:75%;float:left;text-align:center;"><b>Selamat Untuk Para Pemenang '+title+' </b></div>');
        $("#exportButton").jqxButton();
        $("#exportButton").click(function(){
            $(".list_pemenang").jqxGrid('exportdata', 'xls', 'jqxGrid',true, null, true, link+'welcome/export');
        })
    },
    columns: [
        {
          text: '#', sortable: false, filterable: false, editable: false,
          groupable: false, draggable: false, resizable: false,
          datafield: '', columntype: 'number', width: '5%',
          cellsrenderer: function (row, column, value) {
              return "<div style='margin:4px;'>" + (value + 1) + "</div>";
          }
        },
        { text: 'NIK', datafield: 'nik', width: '15%' },
        { text: 'Nama Pemenang', datafield: 'nama', width: '40%' },
        { text: 'Hadiah', datafield: 'hadiah', width: '40%' },
    ]
});

var pathArray = window.location.pathname.match(/utama/);
var getId = window.location.pathname.match(/[0-9]+/);
if(pathArray[0] == 'utama')
{
    $.getJSON(link+'welcome/konfirmasi_txt',function(data){
        if(data === false)
        {
            $.when( asyncEvent() ).then(function(data){
				$('#modal_hadiah').openModal();
				var getDom = $('#modal_hadiah').find('p');
				var nama = data[0].peserta['nama'];
				$(getDom).html('Apakah Pemenang Dengan Nama <b>"'+nama.toUpperCase()+'"</b> Hadir??');
			});
        }
    })
}


function asyncEvent() {
  var dfd = jQuery.Deferred();
 
  $.getJSON(link+'welcome/get_pemenang_hadiah_utama/'+getId,function(data){
    if(data.length > 0)
    {
        dfd.resolve(data);
    }else{
    	dfd.reject(false)
    }
  })
 
  // Return the Promise so caller can't change the Deferred
  return dfd.promise();
}

function konfirmasi_pemenang(x,status)
{
	$.when( asyncEvent() ).then(function(data){
		var id_peserta = data[0].peserta_id;
		var id_hadiah = data[0].hadiah_id;
		var nama = data[0].peserta['nama'];

		$.get(link+'welcome/update_status_pemenang_hadiah_utama/'+status+'/'+id_peserta+'/'+id_hadiah,function(s){
        	console.log(s)
        	if(s === 'update'){
        		console.log('pemenang ditentukan')
        	}
        	else if(s === 'sukses update peserta dan hadiah'){
        		console.log('pemenang ditentukan dan hadiah di hide')
        	}
    		else{
        		console.log('pemenang didelete')
        	}

        	$(".list_pemenang").jqxGrid("updatebounddata");

        	if(typeof data[1] !== 'undefined'){
	        	var nama = data[1].peserta['nama'];
				var getDom = $('#modal_hadiah').find('p');
				$(getDom).html('Apakah Pemenang Dengan Nama <b>"'+nama.toUpperCase()+'"</b> Hadir??');
			}else{
				$('#modal_hadiah').closeModal();
			}
    	});

		/*if(typeof data === 'object'){
			var nama = data[0].peserta['nama'];
			var getDom = $('#modal_hadiah').find('p');
			$(getDom).html('Apakah Pemenang Dengan Nama <b>"'+nama.toUpperCase()+'"</b> Hadir??');
		}else{
			$('#modal_hadiah').closeModal();
		}*/
	});
}