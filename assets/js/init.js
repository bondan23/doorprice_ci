var acak = false;
var parent_id;

var delay = (function(){
  var timer = 0;
  return function(callback, ms){
    clearTimeout (timer);
    timer = setTimeout(callback, ms);
  };
})();

function get_hadiah()
{
	/*var temp ='<div class="col m12"><div data-qty="2" data-kategori="C" class="col push-m4 m4 data_1"><div class="card"><div class="card-image"><img src="http://lorempixel.com/400/300/abstract/?18585"><span style="font-size:12px" class="card-title">Honda Mobilio</span></div><div class="card-content"><p class="center-align"> Honda Mobilio<br>(2 item)</p></div><div class="card-action"><div class="switch center">Pilih Hadiah<br><label>Tidak<input type="checkbox" data-parent="1" onchange="check_pilihan(this)" class="pilih_hadiah"><span class="lever"></span>Ya</label></div></div></div></div></div>';*/
    var temp = '';
	$.getJSON(link+'welcome/get_hadiah',function(data){
		$.each(data, function( index, value ) {
          if(value.hadiah_utama == 1)
          {
              temp += '<div class="col m12"><div class="col push-m4 m4 data_'+value.id+'" data-utama="true" data-kategori="'+value.kategori+'" data-qty="'+value.qty+'"><div class="card"><div class="card-image"><img src="'+value.images+'"><span class="card-title" style="font-size:12px">'+value.nama_hadiah+'</span></div><div class="card-content"><p class="center-align"> '+value.nama_hadiah+'<br/>('+value.qty+' item)</p></div><div class="card-action"><div class="switch center">Pilih Hadiah<br><label>Tidak<input type="checkbox" class="pilih_hadiah" onchange="check_pilihan(this)" data-parent="'+value.id+'"><span class="lever"></span>Ya</label></div></div></div></div></div>';
          }
          else{
                temp += '<div class="col m3 data_'+value.id+'" data-utama="false" data-kategori="'+value.kategori+'" data-qty="'+value.qty+'"><div class="card"><div class="card-image"><img src="'+value.images+'"><span class="card-title" style="font-size:12px">'+value.nama_hadiah+'</span></div><div class="card-content"><p class="center-align"> '+value.nama_hadiah+'<br/>('+value.qty+' item)</p></div><div class="card-action"><div class="switch center">Pilih Hadiah<br><label>Tidak<input type="checkbox" class="pilih_hadiah" onchange="check_pilihan(this)" data-parent="'+value.id+'"><span class="lever"></span>Ya</label></div></div></div></div>';   
          }
		});
        
        if(temp == '')
        {
            $('#list_hadiah').hide();
            $('#download-button').text('LIHAT DAFTAR PEMENANG');
            $('#download-button').removeAttr('onclick');
            $('#download-button').attr('href',link+'board');
            $('.gift_place').html('<h1 class="center blue-text" style="margin-top:1.6em;margin-bottom:1.6em">Semua Hadiah Telah Diundi !</h1>')
        }
        else{
            $('#list_hadiah').show();
            $('#download-button').text('MULAI UNDIAN');
            $('#download-button').attr('onclick','get_winner()');
            $('#download-button').attr('href','javascript:void(0)');
            $('.gift_place').html(temp)
        }
	})
    
}

/*function get_winner()
{
  if(acak == true)
  {
	  $.getJSON('action.php?act=get_undian',function(data){
		if(data.status == "true"){
			  $('#winner').prop('number', 99999999).animateNumber(
				{
				  number: data.nomor_undian,
				  'font-size': '50px',
				},
				1000,
				function() {
					$.ajax({
					  dataType: "json",
					  url: 'action.php?act=update_db',
					  data: {id:data.winner_id},
					  type:"POST",
					  success: function(res)
					  {
						//$('#msg').html(res.msg);
						//var qty = $('.data_'+parent_id).attr('data-qty');
						//console.log($('.data_'+parent_id).attr('data-qty'));
						
						var x = confirm('Apakah Pemenang Hadir ??');
						if(x == true)
						{
							$.ajax({
								url:'action.php?act=update_hadiah&id='+parent_id,
								type:'GET',
								dataType:'json',
								success:function(data)
								{
									var qty = data.qty;
									
									if(qty == 0){
										$('.data_'+parent_id).fadeOut('slow',function(){
											$(this).remove();
											acak = false;
											$('.pilih_hadiah:not(:checked)').removeAttr('disabled');
										});
									}
									else{
										$('.data_'+parent_id).attr('data-qty',qty);
										console.log(data.qty);
										console.log('silakan undi pemenang lagi');
									}
								}
							});
						}
					  }
					});
				}
			  );
		}
		else{
			$('#msg').html('Semua Pemenang Telah di Undi,Terima Kasih...!!');	
		}
	  });
  }
  else{
	  return alert('Pilih Hadiah Terlebih Dahulu');
  }
}*/


$('.preloader').hide();

function get_winner()
{
    if(acak == true)
    {
        var ini = $('.pilih_hadiah:checked');
        var parent = ".data_"+ini.attr('data-parent');
        
        var hadiah_id = ini.attr('data-parent');
        var qty = $(parent).data('qty');
        var kategori = $(parent).data('kategori');
        var hadiah_utama = $(parent).data('utama');
        $('.preloader').css({
            "height":"100%",
        });
        $('.preloader').fadeIn(100);
        
        $.post(link+'welcome/get_pemenang',{hadiah_id:hadiah_id,qty:qty,kategori:kategori},function(a){
            if(a == 'true')
            {
                if(hadiah_utama == true)
                {
                    var param = '/utama'
                }
                else{
                    var param = null;
                }

                delay(function(){
                    window.location=link+"welcome/hasil_undi/"+hadiah_id+param
                },1000)
            }
            else{
                $('.preloader').fadeOut(100,function(){
                    alert('Data Peserta Valid Kurang Dari Jumlah Hadiah...');
                });
            }
        })
    }
    else{
      return alert('Pilih Hadiah Terlebih Dahulu');
    }
}

function check_pilihan(x)
{
	if($('.pilih_hadiah').is(":checked") == true)
	{
		parent_id = $(x).attr('data-parent');
		acak = true;
		var hitung = $('.pilih_hadiah:checked').length;
		if(hitung == 1)
		{
			$('.pilih_hadiah:not(:checked)').attr('disabled','true');
		}
	}
	else{
		acak = false;
		$('.pilih_hadiah:not(:checked)').removeAttr('disabled');
	}
	
	console.log(acak);
}

$(document).ready(function(){
	check_pilihan();
	get_hadiah();
	$('.button-collapse').sideNav();
})
	
	