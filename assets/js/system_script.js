var data = [
	{
		"id": 1,
		"nama": "Noble Nelson",
		"hadiah": "Laptop Asus"
	},
	{
		"id": 2,
		"nama": "Christine Salazar",
		"hadiah": "Laptop Asus"
	},
	{
		"id": 3,
		"nama": "Rigel K. Dalton",
		"hadiah": "Laptop Asus"
	},
	{
		"id": 4,
		"nama": "Phillip T. Frazier",
		"hadiah": "Laptop Asus"
	},
	{
		"id": 5,
		"nama": "Jasmine Wilcox",
		"hadiah": "Laptop Asus"
	},
	{
		"id": 6,
		"nama": "Acton D. Lambert",
		"hadiah": "Laptop Asus"
	},
	{
		"id": 7,
		"nama": "Kyla Gardner",
		"hadiah": "Laptop Asus"
	},
	{
		"id": 8,
		"nama": "Kevyn C. Hester",
		"hadiah": "Laptop Asus"
	},
	{
		"id": 9,
		"nama": "Ishmael I. Hayes",
		"hadiah": "Laptop Asus"
	},
	{
		"id": 10,
		"nama": "Levi Mullins",
		"hadiah": "Laptop Asus"
	},
	{
		"id": 11,
		"nama": "Quentin M. Mendez",
		"hadiah": "Laptop Asus"
	},
	{
		"id": 12,
		"nama": "Lydia Lewis",
		"hadiah": "Laptop Asus"
	},
	{
		"id": 13,
		"nama": "Elliott Kerr",
		"hadiah": "Laptop Asus"
	},
	{
		"id": 14,
		"nama": "Ciaran Shelton",
		"hadiah": "Laptop Asus"
	},
	{
		"id": 15,
		"nama": "Samson R. Scott",
		"hadiah": "Laptop Asus"
	},
	{
		"id": 16,
		"nama": "Wesley T. Bird",
		"hadiah": "Laptop Asus"
	},
	{
		"id": 17,
		"nama": "Jackson M. Charles",
		"hadiah": "Laptop Asus"
	},
	{
		"id": 18,
		"nama": "Scarlett A. Glenn",
		"hadiah": "Laptop Asus"
	},
	{
		"id": 19,
		"nama": "Garth Barber",
		"hadiah": "Laptop Asus"
	},
	{
		"id": 20,
		"nama": "Yasir Lowery",
		"hadiah": "Laptop Asus"
	},
];

var source =
{
    datatype: 'json',
    datafields: [
        {name: 'nik', type:'string'},
        {name: 'nama', type:'string'},
        {name: 'kategori', type:'string'},
        {name: 'department', type:'string'},
    ],
    root:'data',
    id: 'id',
    url: link+'secure/import/data_peserta'
};

/*var source =
{
    localdata: data,
    datatype: "array"
};*/

var dataAdapter = new $.jqx.dataAdapter(source, {
    loadComplete: function (data) { },
    loadError: function (xhr, status, error) { },
    formatData: function (data) {
        data.q = $("#searchField").val();
        return data;
    }
});

$(".data_peserta").jqxGrid(
{
    width:'100%',
    source:dataAdapter,
    columnsresize: true,
    showtoolbar: true,
    autoheight: true,
    sortable: true,
    pageable: true,
    renderToolbar : function(toolbar)
    {
        var me = this;
        var title = dataAdapter.loadedData.title;
        var container = $("<div style='margin: 5px;'></div>");
        var label = $("<label style='margin-left:5px;'>Search</label>");
        var input = $("<input class='jqx-input jqx-widget-content jqx-rc-all' id='searchField' placeholder='NIK/Nama' type='text' style='padding:3px;margin-left:10px;height: 23px;width: 223px;' />");
        toolbar.append(container);
        toolbar.append(label);
        toolbar.append(input);
        
        input.on('keyup', function (event) {
            if (input.val().length >= 4) {
                $(".data_peserta").jqxGrid('updatebounddata');
            }
            else{
                $(".data_peserta").jqxGrid('updatebounddata');
            }
        });
    },
    columns: [
        {
          text: '#', sortable: false, filterable: false, editable: false,
          groupable: false, draggable: false, resizable: false,
          datafield: '', columntype: 'number', width: '5%',
          cellsrenderer: function (row, column, value) {
              return "<div style='margin:4px;'>" + (value + 1) + "</div>";
          }
        },
        { text: 'NIK', datafield: 'nik', width: '15%' },
        { text: 'Nama Peserta', datafield: 'nama', width: '40%' },
        { text: 'Department', datafield: 'department', width: '30%' },
        { text: 'Kategori', datafield: 'kategori', width: '10%' },
    ]
});