var source =
{
    datatype: 'json',
    datafields: [
        {name: 'id', type:'string'},
        {name: 'kategori', type:'string'}
    ],
    root:'data',
    id: 'id',
    url: link+'secure/kategori/data_kategori'
};

/*var source =
{
    localdata: data,
    datatype: "array"
};*/

var dataAdapter = new $.jqx.dataAdapter(source, {
    loadComplete: function (data) { },
    loadError: function (xhr, status, error) { }    
});


$(".data_kategori").jqxGrid(
{
    width:'50%',
    source:dataAdapter,
    columnsresize: true,
    showtoolbar: true,
    autoheight: true,
    sortable: true,
    pageable: true,
    renderToolbar : function(toolbar)
    {
        var me = this;
			var container = $("<div style='margin: 5px;'></div>");
			toolbar.append(container);
			container.append('<input id="addrowbutton" type="button" value="+ Input Kategori" />');
			$("#addrowbutton").jqxButton();
			$("#addrowbutton").on("click",function(){
               /* $("#save").val('Save');
                $("#method").val('save');
                
				$("input[name='nama']").val('');
                $("input[type='file']").val('');
				
				
				$("input[name='id']").remove();
				$("input[name='oldphoto']").remove();
				*/
                $("input[name='nama_kategori']").val('');
                
				$("#header-form").html("Tambah Kategori");
                
                $("input[name='id']").remove();
                //$("input[name='oldimages']").remove();
                
                $("input[name='method']").val('post');
				$("#jqxwindow").show();
				$("#jqxwindow").jqxWindow('open')
				$("#jqxwindow").jqxWindow('focus')
			});
    },
    columns: [
        {
          text: '#', sortable: false, filterable: false, editable: false,
          groupable: false, draggable: false, resizable: false,
          datafield: '', columntype: 'number', width: '10%',
          cellsrenderer: function (row, column, value) {
              return "<div style='margin:4px;'>" + (value + 1) + "</div>";
          }
        },
        { text: 'Kategori', datafield: 'kategori', width: '50%' },
        { text: 'Delete', datafield: 'delete', width:'40%',columntype: 'button', cellsrenderer: function () {
                return "Delete";
            },buttonclick: function (row) {
                var r = confirm('Ingin Hapus Data Ini ?');
                var id = $(".data_kategori").jqxGrid('getrowid', row);
                if(r == true)
                {
                    $.ajax({
                        url:link+"secure/kategori/delete",
                        data:{id:id},
                        method:"POST",
                        success:function()
                        {
                            $(".data_kategori").jqxGrid("updatebounddata");
                        }
                    })
                }
            }
        }
    ]
});

$('#form_kategori').on("submit",function(e){
    e.preventDefault();

    var formData = new FormData(this);
    
    $.ajax({
        url: link+"secure/kategori/post",
        type: "POST",
        data:formData,
        dataType:"json",
        processData: false,
        contentType: false,
        success: function (res) {
            if(res.status == true)
            {
                alert(res.msg);
                $("#jqxwindow").jqxWindow('close');
                $("#jqxwindow").hide();
                $(".data_kategori").jqxGrid("updatebounddata");
            }
            else{
                alert(res.msg);
            }
        },
        error: function(res){
            if(res.status === 500){
                alert('Duplicate Data Kategori');
            }
        }
    });
})

$("#jqxwindow").jqxWindow({
    width: 700,
    minHeight: 200,
    resizable: false,  
    isModal: true, 
    autoOpen: false,
    cancelButton: $("#cancel"), 
    modalOpacity: 0.01           
});