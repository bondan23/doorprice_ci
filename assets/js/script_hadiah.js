var source =
{
    datatype: 'json',
    datafields: [
        {name: 'id', type:'string'},
        {name: 'nama_hadiah', type:'string'},
        {name: 'qty', type:'string'},
        {name: 'kategori', type:'string'},
        {name: 'images', type:'string'},
        {name: 'hadiah_utama', type:'string'}
    ],
    root:'data',
    id: 'id',
    url: link+'secure/hadiah/data_hadiah'
};

/*var source =
{
    localdata: data,
    datatype: "array"
};*/

var dataAdapter = new $.jqx.dataAdapter(source, {
    loadComplete: function (data) { },
    loadError: function (xhr, status, error) { }    
});

$('.select_kategori').material_select();

$('.image-editor').cropit();

$(".data_hadiah").jqxGrid(
{
    width:'100%',
    source:dataAdapter,
    columnsresize: true,
    showtoolbar: true,
    autoheight: true,
    sortable: true,
    pageable: true,
    renderToolbar : function(toolbar)
    {
        var me = this;
			var container = $("<div style='margin: 5px;'></div>");
			toolbar.append(container);
			container.append('<input id="addrowbutton" type="button" value="+ Input Hadiah" />');
			$("#addrowbutton").jqxButton();
			$("#addrowbutton").on("click",function(){
               /* $("#save").val('Save');
                $("#method").val('save');
                
				$("input[name='nama']").val('');
                $("input[type='file']").val('');
				
				
				$("input[name='id']").remove();
				$("input[name='oldphoto']").remove();
				*/
                $("input[name='nama_hadiah']").val('');
                $("input[name='qty']").val('');
                $("input[type='file']").val('');
                $(".file-path").val('');
                $('.cropit-preview-image').attr('src','');
                $('input[type=checkbox]').prop('checked',false);
                
				$("#header-form").html("Tambah Hadiah");
                
                $("input[name='id']").remove();
                //$("input[name='oldimages']").remove();
                
                $("input[name='method']").val('post');
				$("#jqxwindow").show();
				$("#jqxwindow").jqxWindow('open')
				$("#jqxwindow").jqxWindow('focus')
			});
    },
    columns: [
        {
          text: '#', sortable: false, filterable: false, editable: false,
          groupable: false, draggable: false, resizable: false,
          datafield: '', columntype: 'number', width: '5%',
          cellsrenderer: function (row, column, value) {
              return "<div style='margin:4px;'>" + (value + 1) + "</div>";
          }
        },
		
        { text: 'ID', datafield: 'id', width: '5%',cellsrenderer:function(row, columnfield, value, defaulthtml, columnproperties, rowdata){
			return "<div class='jqx-grid-cell-left-align' style='margin-top: 4px;'><a target='_blank' href='../welcome/hasil_undi/"+rowdata.id+"'>View</a></div>";
			}
		},
        { text: 'Nama Hadiah', datafield: 'nama_hadiah', width: '30%'},
        { text: 'Kategori', datafield: 'kategori', width: '10%' },
        { text: 'QTY', datafield: 'qty', width: '5%' },
        { text: 'Images', datafield: 'images', width: '10%',cellsrenderer:function(row, columnfield, value, defaulthtml, columnproperties, rowdata){
            return "<div class='jqx-grid-cell-left-align' style='margin-top: 4px;'><a target='_blank' href='../"+rowdata.images+"'>Click</a></div>";
        }},
        { text: 'Hadiah Utama', datafield:'hadiah_utama',width:'15%',columntype: 'button',cellsrenderer:function(row, columnfield, value, defaulthtml, columnproperties, rowdata){
            if(value == 1)
            {
                return "Hadiah Utama";
            }
            else{
                return "Set";
            }
        },buttonclick:function(row){
            var id = $(".data_hadiah").jqxGrid('getrowid', row);
            $.post(link+'secure/hadiah/set_hadiah_utama',{id:id},function(){
                $(".data_hadiah").jqxGrid("updatebounddata");
            })
        }},
        { text: 'Edit', datafield: 'edit', width:'10%',columntype: 'button', cellsrenderer: function () {
					return "Edit";
        },buttonclick: function (row) {
            var id = $(".data_hadiah").jqxGrid('getrowid', row);
            var data = $(".data_hadiah").jqxGrid('getrowdata', row);
           
            $('#form_hadiah').trigger('reset');
            $("input[name='method']").val('patch');
            $("#header-form").html("Tambah Hadiah");
            //data
            $("label[for='nama_hadiah']").addClass('active');
            $("input[name='nama_hadiah']").val(data.nama_hadiah);
            $("label[for='qty']").addClass('active');
            $("input[name='qty']").val(data.qty);
            
            var hitung = $("#form_hadiah").find('input[name="id"]').length;
            
            if(hitung == 0)
            {
                $("input[name='method']").after('<input type="hidden" name="id" value="'+id+'"/>');
            }
            else{
                $('input[name="id"]').val(id);
            }
            //$("input[name='method']").after('<input type="hidden" name="oldimages" value="'+data.images+'"/>');
            
            var selected = data.kategori;
            var clean = selected.replace(/'/g,"")
            var explode = clean.split(",");
            $.each(explode,function(k,v){
                var Selector = v.toLowerCase();
                $('#'+Selector).prop('checked','true')
            })
            /*var arr = ["A","B","C","D","E"];
            var index = $.inArray( selected, arr );
            var get = $('.select_kategori option:eq('+index+1+')');
            get.attr('selected','true')
            //console.log(get.attr('selected','true'))
            console.log($('input[value="Pilih Kategori"]').val(selected));*/
            
            $("#jqxwindow").show();
            $("#jqxwindow").jqxWindow('open')
            $("#jqxwindow").jqxWindow('focus')
        }},
        { text: 'Delete', datafield: 'delete', width:'10%',columntype: 'button', cellsrenderer: function () {
					return "Delete";
        },buttonclick: function (row) {
            var r = confirm('Ingin Hapus Data Ini ?');
            var id = $(".data_hadiah").jqxGrid('getrowid', row);
            if(r == true)
            {
                $.ajax({
                    url:link+"secure/hadiah/delete",
                    data:{id:id},
                    method:"POST",
                    success:function()
                    {
                        $(".data_hadiah").jqxGrid("updatebounddata");
                    }
                })
            }
        }
        },
    ]
});

$('#form_hadiah').on("submit",function(e){
    e.preventDefault();
    
    var imageData = $('.image-editor').cropit('export');
    $('.hidden-image-data').val(imageData);
    
    // Print HTTP request params
    //var formValue = $(this).serialize();
    //$('.hidden-image-data').val(imageData);
    
    var formData = new FormData(this);
    
    $.ajax({
         xhr: function() {
            var xhr = new window.XMLHttpRequest();

            xhr.upload.addEventListener("progress", function(evt) {
              if (evt.lengthComputable) {
                var percentComplete = evt.loaded / evt.total;
                percentComplete = parseInt(percentComplete * 100);
                console.log(percentComplete);
                var percent = percentComplete+"%";

                /*$('#progress_value').attr("aria-valuenow",percentComplete)
                $('#progress_value').css("width",percent)*/
                $('.determinate').css("width",percentComplete+"%")

              }
            }, false);

            return xhr;
        },
        url: link+"secure/hadiah/post",
        type: "POST",
        data:formData,
        dataType:"json",
        processData: false,
        contentType: false,
        success: function (res) {
            if(res.status == true)
            {
                alert(res.msg);
                $("#jqxwindow").jqxWindow('close');
                $("#jqxwindow").hide();
                $(".data_hadiah").jqxGrid("updatebounddata");
            }
            else{
                alert(res.msg);
            }
            
            $('.determinate').css("width",0+"%");
        }
    });
})

$("#jqxwindow").jqxWindow({
    width: 700,
    minHeight: 200,
    resizable: false,  
    isModal: true, 
    autoOpen: false,
    cancelButton: $("#cancel"), 
    modalOpacity: 0.01           
});