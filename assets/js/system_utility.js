$(document).ready(function() {
    //toggle `popup` / `inline` mode
    
    $.fn.editable.defaults.ajaxOptions = {type: "post"}
    $.fn.editable.defaults.mode = 'inline';     
    
    $('#header').editable({
        url:link+'secure/header/edit',
        name:'text',
        pk:$(this).attr('data-pk'),
        type: 'text',
        tpl:'<input type="text" style="text-align:center;font-size:3em"/>',
        showbuttons:false,
        success: function(response, newValue) {
            console.log(response)
        }   
    });
    
    $('#tajuk').editable({
        url:link+'secure/header/edit',
        name:'text',
        pk:$(this).attr('data-pk'),
        type: 'text',
        tpl:'<input type="text" style="text-align:center;font-size:3em"/>',
        showbuttons:false,
        success: function(response, newValue) {
            console.log(response)
        }   
    });
    
    
});