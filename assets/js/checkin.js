var delay = (function(){
    var timer = 0;
    return function(callback, ms){
        clearTimeout (timer);
        timer = setTimeout(callback, ms);
    };
})();


var source =
{
    datatype: 'json',
    datafields: [
        {name: 'nik', type:'string'},
        {name: 'nama', type:'string'},
        {name: 'kategori', type:'string'},
        {name: 'updated_at', type:'string'},
    ],
    root:'data',
    id: 'id',
    url: link+'secure/checkin/peserta_hadir'
};

/*var source =
{
    localdata: data,
    datatype: "array"
};*/

var dataAdapter = new $.jqx.dataAdapter(source, {
    loadComplete: function (data) { },
    loadError: function (xhr, status, error) { },
    formatData: function (data) {
        data.q = $("#searchField").val();
        return data;
    }
});

$(".data_peserta_hadir").jqxGrid(
{
    width:'100%',
    source:dataAdapter,
    columnsresize: true,
    showtoolbar: true,
    autoheight: true,
    sortable: false,
    pageable: true,
    renderToolbar : function(toolbar)
    {
        //var title = dataAdapter.loadedData.title;
        var container = $('<div class="center" style="margin-top:3px;"></div>');
        toolbar.append(container);
        container.append('<b>List Peserta Hadir</b>');
    },
    columns: [
        {
          text: '#', sortable: false, filterable: false, editable: false,
          groupable: false, draggable: false, resizable: false,
          datafield: '', columntype: 'number', width: '5%',
          cellsrenderer: function (row, column, value) {
              return "<div style='margin:4px;'>" + (value + 1) + "</div>";
          }
        },
        { text: 'NIK', datafield: 'nik', width: '15%' },
        { text: 'Nama Peserta', datafield: 'nama', width: '40%' },
        { text: 'Kategori', datafield: 'kategori', width: '20%' },
        { text: 'Checkin Time', datafield: 'updated_at', width: '20%' },
    ]
});


$('.barcode_check').focus();
/*$('.barcode_check').on('keydown',function(){
    var len = $(this).val().length;
    if(len > 5)
    {
        var val = $(this).val().replace(/\s/g,'');
        //console.log(val);
        delay(function(){
           $.getJSON(link+'secure/checkin/check/'+val,function(a){
               if(a.status == true)
               {
                   console.log('Sukses Checkin');
                   $('.barcode_check').val('');
                   $(".data_peserta_hadir").jqxGrid("updatebounddata");
               }
               else{
                   $('.barcode_check').val('');
               }
           })
        },1000);
    }
})*/